#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDateTime>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // declare the range and format of the seconds bar. Then set first time
    ui->secondsBar->setRange(0, 60);
    ui->secondsBar->setFormat("%v seconds");
    updateTimer();

    // create timer object that will run updateTimer every second
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout,this, &MainWindow::updateTimer);
    timer->start(1000);
}

MainWindow::~MainWindow()
{
    delete ui;
}


// Function to run every x seconds
void MainWindow::updateTimer()
{
    // get current time object
    QTime time = QTime::currentTime();

    //format to hour : minute
    QString timeString = time.toString("hh:mm");

    // If time is even, don't show colon
    timeString[2] = (time.second() % 2) == 0 ? ' ' : ':';

    // Set LCD to time and seconds bar to seconds
    ui->timeLCD->display(timeString);
    ui->secondsBar->setValue(time.second());
}
